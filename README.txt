-- REQUIREMENTS --

* Your Adobe Test&Target mbox.js file.

-- INSTALLATION --

* Copy the testandtarget module directory to your sites/all/modules directory, so it
  is located in sites/all/modules/testandtarget/.

* Download the mbox.js file from your Adobe Test&Target installation at:

    Configuration > mbox.js > Download

* Put the downloaded js file into the directory:

    /sites/all/libraries/testandtarget/mbox.js

* Enable the module at Administration >> Modules.
